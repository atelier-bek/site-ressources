# Ressources
La partie ressources du site.

## Usage

URL

Titre (auteur - oeuvre / objet )

Description

Tags
1 (média) = livres - web - etc
2 ( catégories) = art - technique - etc
3  ( sous-catégories) = peinture - vidéo


Exemple de post

URL=
http://cloud.atelier-bek.be/index.php/apps/files/?dir=/pdf&fileid=3858#pdfviewer

Titre=
Robert Cecil Martin - Coder proprement

Description=
Parce que le code sale peut fonctionner et que le code propre est encore plus cool

Tags=
1livre 2technique 3code

## Todo

-

## notes

22.02

Ressources/ bibliothèque

organisation en 6 parties

Ressources — Git—Typothèque—Log—Archives

entré par ordre :

- Alphabétique
- Hasard
- Tag

## Techniques
- shaarli? (ce qui signifie uniquement des hyperliens) ← on part là-dessus
- ~~un pad?~~

## Références
### Systèmes de classification
- [Classification décimale de Dewey](https://fr.wikipedia.org/wiki/Classification_d%C3%A9cimale_de_Dewey)
- [Classification décimale universelle](https://fr.wikipedia.org/wiki/Classification_d%C3%A9cimale_universelle)  
### Sites référents
- [Subjective encyclopedia](https://subjective-encyclopedia.ch/encyclopedia/letter/o#opfer-des-zivilisatorischen-fortschritts)
- [Interstices](http://interstices.io/)
