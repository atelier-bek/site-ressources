$(document).ready(function(){
  var filters   = $('#filters');
  filterEl      = filters.find('li').not('li:nth-of-type(1)');
  var list      = $('#list');
  var content   = $('#content');
  var permalink = $('a').attr('title', 'Permalink').not('.link');

  permalink.hide();
  goToContent(list, content);
  filter(filters, list, content);
  spanLetters($('.link'));
  spanLetters(filterEl);
  skewTxt($('.link'));
  filterEl.mouseenter(function(){
     scaleTxt($(this));
  }).mouseleave(function(){
    $(this).not('.clicked').children('span').css({'transform': 'scaleY(1)'});
  })

})

$(window).on('load', function(){

  sectionH();

})

$(window).resize(function(){

  sectionH();

})
