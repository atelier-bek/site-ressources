function sectionH(){
  var sections  = $('section');
  var windowH   = $(window).height();

  sections.height(windowH);
}

function goToContent(list, content){
  list.children('ul').children('ul').children('li').click(function(){
    var theClass = $(this).attr('class').split(' ')[0];
    var elToGo = content.children('ul').children('.'+theClass);
    content.scrollTo(elToGo, 500);
  })
}

function filter(filters, list, content){
  filters.find('li').click(function(){
    var theClass = $(this).attr('class').split(' ')[0];
    if ($(this).hasClass('clicked')) {
      $(this).removeClass('clicked');
      content.find('li').not('.' + theClass).show();
      list.find('.contentList li').not('.' + theClass).show();
    } else{
      $(this).addClass('clicked');
      // var contentClass = content.find('li.'+ theClass).attr('class').split(' ');
      // for (var i = 0; i < contentClass.length; i++) {
      //   if (filters.find('li').hasClass(contentClass[i])) {
      //     console.log(contentClass[i]);
      //     filters.find('li.' + contentClass[i]).css({opacity: '.5'});
      //   }
      // }
    // console.log(contentClass);
    content.find('li').not('.' + theClass).hide();
    list.find('.contentList li').not('.' + theClass).hide();
      // content.find('.' + theClass).show();
      // list.find('.' + theClass).show();
    }
  })
}


function spanLetters(el){
  el.each(function(){
    var letters = $(this).html().split('');
    for (var i = 0; i < letters.length; i++) {
      if (letters[i] == ' ') {
        letters[i] = '<span>&nbsp;</span>';
      }

      letters[i] = '<span>' + letters[i] + '</span>';
    }
    var newLink = letters.join('');
    $(this).html(newLink);
  })
}

function skewTxt(el){

  el.each(function(){
    var spanLink = $(this).children('span');
    var i = 0;
    spanLink.each(function(){
      $(this).css({
        'display': 'inline-block',
        'transform': 'skewY(' + i + 'deg)'});
        if (i < 35) {
          i = i + 1.25;
        }
    })
  })
}

function spaceTxt(el){
  el.each(function(){
    var letters = $(this).find('span');
    letters.removeAttr('style');
    var i = 0;
    letters.each(function(){
      $(this).css({'letter-spacing': i + 'px'})
        i = i + 0.75;
    })
  })
}

function scaleTxt(el){
  el.each(function(){
    var letters = $(this).find('span');
    letters.removeAttr('style');
    var i = 0.5;
    letters.each(function(){
      $(this).css({'transform': 'scaleY(' + i + ')'})
        i = i + 0.25;
    })
  })
}
