<?php

  $abc        = range('A', 'Z');
  $titles     = array();
  $categories = array();

  foreach($content->channel->item as $entry) {
    array_push($titles, $entry->title);
    array_push($categories, $entry->category);
  }
?>

<section id="list">
  <ul>
    <?php foreach ($abc as $letter): ?>
      <li class="letter <?= $letter ?>"><?= $letter ?></li>
      <?php
        foreach ($titles as $i => $title):
          $titleSlug = slug($title);
          if (strtolower($letter) == strtolower(substr($title, 0, 1))) {
          ?>
            <ul class="contentList">
              <li class="<?= $titleSlug ?> <?php foreach($categories[$i] as $category) echo substr($category, 1).' '; ?>"><?php echo $title; ?></li>
            </ul>
          <?php
          }
        endforeach
      ?>
    <?php endforeach ?>
  </ul>
</section>
