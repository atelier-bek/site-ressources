<section id="filters">
  <?php
    $types = $categories1 = $categories2 = array();
    getAndShowFilters($content, $types, 'type', 1);
    getAndShowFilters($content, $categories1, 'domaine', 2);
    getAndShowFilters($content, $categories2, 'catégorie', 3);
  ?>
</section>
